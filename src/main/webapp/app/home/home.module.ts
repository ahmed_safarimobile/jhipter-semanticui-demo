import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipterSharedModule } from '../shared';

import { HOME_ROUTE, HomeComponent } from './';
import {SuiMultiSelect, SuiSelectOption} from "ng2-semantic-ui/dist";
import {SuiModule} from "ng2-semantic-ui";

@NgModule({
    imports: [
        SuiModule,
        JhipterSharedModule,
        RouterModule.forChild([ HOME_ROUTE ])
    ],
    declarations: [
        HomeComponent
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipterHomeModule {}
